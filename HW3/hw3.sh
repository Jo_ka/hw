#!/bin/bash

dire=$1
files=`ls $dire`

for fil1 in $files; do
	for fil2 in $files; do
		if [[ -z $(diff $fil1 $fil2) && $fil1 != $fil2 ]]; then
			echo $fil1 $fil2
		fi 
	done
done